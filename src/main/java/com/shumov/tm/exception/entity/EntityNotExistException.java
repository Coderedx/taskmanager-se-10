package com.shumov.tm.exception.entity;

public class EntityNotExistException extends Exception {

    public EntityNotExistException() {
        super("OBJECT WITH THIS ID DOES NOT EXIST!");
    }
}
