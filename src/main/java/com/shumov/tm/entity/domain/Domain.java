package com.shumov.tm.entity.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
public class Domain implements Serializable {

    @Nullable
    private List<Project> projects = new ArrayList<>();
    @Nullable
    private List<Task> tasks = new ArrayList<>();
    @Nullable
    private List<User> users = new ArrayList<>();

}
