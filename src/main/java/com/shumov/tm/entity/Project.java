package com.shumov.tm.entity;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.enumerate.ExecutionStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.*;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Project extends AbstractEntity implements Entity, Serializable {
    @NotNull
    private String name = "";
    @NotNull
    private String description = "";
    @NotNull
    private Date dateStart = new Date();
    @NotNull
    private Date dateFinish = new Date();
    @NotNull
    private ExecutionStatus status = ExecutionStatus.PLANNED;

    public Project(@NotNull final String name, @NotNull final String ownerId)
    {
        this.name = name;
        this.ownerId = ownerId;
    }
}
