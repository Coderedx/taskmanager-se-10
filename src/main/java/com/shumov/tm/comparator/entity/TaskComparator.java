package com.shumov.tm.comparator.entity;

import com.shumov.tm.entity.Task;
import com.shumov.tm.enumerate.SortMethod;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public class TaskComparator implements Comparator<Task> {

    private @NotNull SortMethod method;

    public TaskComparator(@NotNull final SortMethod method) {
        this.method = method;
    }

    @Override
    public int compare(@NotNull final Task o1, @NotNull final Task o2) {
        switch (method){
            case BY_CREATE_DATE:
                return o1.getDateCreated().compareTo(o2.getDateCreated());
            case BY_START_DATE:
                return o1.getDateStart().compareTo(o2.getDateStart());
            case BY_FINISH_DATE:
                return o1.getDateFinish().compareTo(o2.getDateFinish());
            case BY_STATUS:
                return Integer.compare(o1.getStatus().getNumber(),o2.getStatus().getNumber());
            default: return 0;
        }
    }
}
