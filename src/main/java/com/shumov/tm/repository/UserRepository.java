package com.shumov.tm.repository;

import com.shumov.tm.api.repository.IUserRepository;
import com.shumov.tm.entity.User;
import com.shumov.tm.exception.entity.EntityIsAlreadyExistException;
import com.shumov.tm.exception.entity.EntityListIsEmptyException;
import com.shumov.tm.exception.entity.EntityNotExistException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@NoArgsConstructor
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @NotNull
    public final User findOne(@NotNull final String login) throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty) throw new EntityListIsEmptyException();
        @NotNull final Collection<User> usersList = entityMap.values();
        for (@NotNull final User userDb : usersList){
            @Nullable final String userDbLogin = userDb.getLogin();
            if (login.equals(userDbLogin)){
                return userDb;
            }
        }
        throw new EntityNotExistException();
    }

    @Override
    public final void persist(@NotNull final User user) throws Exception {
        @NotNull final String userId = user.getId();
        final boolean mapContainsKey = entityMap.containsKey(userId);
        if(mapContainsKey) throw new EntityIsAlreadyExistException();
        @NotNull final Collection<User> usersList = entityMap.values();
        for(@NotNull final User userDb : usersList){
            @Nullable final String userLogin = user.getLogin();
            @Nullable final String userDbLogin = userDb.getLogin();
            if(userLogin != null && userLogin.equals(userDbLogin)) throw new EntityIsAlreadyExistException();
        }
        entityMap.put(userId, user);
    }
}
