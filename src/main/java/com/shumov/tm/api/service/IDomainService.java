package com.shumov.tm.api.service;

import com.shumov.tm.entity.domain.Domain;
import org.jetbrains.annotations.Nullable;

public interface IDomainService {

    void load(@Nullable Domain domain) throws Exception;

    void export(@Nullable Domain domain) throws Exception;
}
