package com.shumov.tm.api.repository;

import com.shumov.tm.entity.Task;

public interface ITaskRepository extends IRepository<Task> {
}
