package com.shumov.tm.command.system;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import com.shumov.tm.api.service.IDomainService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.domain.Domain;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class FasterSaveXMLCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "faster-save-xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save all data in .xml file by Faster";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVE FASTER-XML DATA]");
        if(serviceLocator == null) return;
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_1_1, true);
        @NotNull final ObjectWriter objectWriter = xmlMapper.writerWithDefaultPrettyPrinter();
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);
        @NotNull final String xml = objectWriter.writeValueAsString(domain);
        @NotNull final byte[] data = xml.getBytes(StandardCharsets.UTF_8);
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        if (!workingFolder.exists()) workingFolder.mkdir();
        @NotNull final File domainFile = new File(workingFolder, "domain.xml");
        Files.write(domainFile.toPath(), data);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }
}
