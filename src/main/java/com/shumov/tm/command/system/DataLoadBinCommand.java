package com.shumov.tm.command.system;

import com.shumov.tm.api.service.IDomainService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.domain.Domain;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

import java.io.*;


public class DataLoadBinCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "data-load-bin";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load all data from .bin file";
    }

    @Override
    public void execute() throws Exception {
        loadData();
    }

    @Override
    public void initRoles() {

        roleTypes.add(UserRoleType.ADMIN);
    }

    private void loadData() throws Exception {
        System.out.println("[LOAD DATA]");
        if(serviceLocator == null) return;
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        @NotNull final File domainFile = new File(workingFolder, "domain.bin");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(domainFile);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        serviceLocator.setCurrentUser(serviceLocator.getGuestUser());
        System.out.println("[OK]");
    }

}
