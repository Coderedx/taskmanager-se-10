package com.shumov.tm.command.system;

import com.shumov.tm.api.service.IDomainService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.domain.Domain;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

import java.io.*;


public class DataSaveBinCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "data-save-bin";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save all data in .bin file";
    }

    @Override
    public void execute() throws Exception {
        saveData();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void saveData() throws Exception {
        System.out.println("[SAVE BINARY DATA]");
        if(serviceLocator == null) return;
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        if (!workingFolder.exists()) workingFolder.mkdir();
        @NotNull final File domainFile = new File(workingFolder, "domain.bin");
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(domainFile);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        if(serviceLocator == null) return;
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        @NotNull final Domain domain = new Domain();
        domainService.export(domain);
        objectOutputStream.writeObject(domain);
        System.out.println("[OK]");
    }
}
