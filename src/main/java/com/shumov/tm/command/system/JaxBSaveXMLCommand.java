package com.shumov.tm.command.system;

import com.shumov.tm.api.service.IDomainService;

import com.shumov.tm.command.AbstractCommand;

import com.shumov.tm.entity.domain.Domain;
import com.shumov.tm.enumerate.UserRoleType;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.HashMap;


public class JaxBSaveXMLCommand extends AbstractCommand {


    @Override
    public @NotNull String command() {
        return "jaxb-save-xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save all data in .xml file by JAX-B";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVE JAXB-XML DATA]");
        if(serviceLocator == null) return;
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, new HashMap());
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        if (!workingFolder.exists()) workingFolder.mkdir();
        @NotNull final File domainFile = new File(workingFolder, "domain.xml");
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);
        marshaller.marshal(domain, domainFile);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }
}
