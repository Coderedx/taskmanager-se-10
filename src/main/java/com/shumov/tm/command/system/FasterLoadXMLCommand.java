package com.shumov.tm.command.system;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.shumov.tm.api.service.IDomainService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.domain.Domain;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public class FasterLoadXMLCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "faster-load-xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load all data from .xml file by Faster";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOAD FASTER-XML DATA]");
        if(serviceLocator == null) return;
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        @NotNull final File domainFile = new File(workingFolder, "domain.xml");
        @NotNull final Domain domain = xmlMapper.readValue(domainFile,Domain.class);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        serviceLocator.setCurrentUser(serviceLocator.getGuestUser());
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }
}
