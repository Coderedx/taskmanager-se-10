package com.shumov.tm.command.system;

import com.shumov.tm.api.service.IDomainService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.domain.Domain;
import com.shumov.tm.enumerate.UserRoleType;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.jetbrains.annotations.NotNull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.HashMap;

public class JaxBLoadXMLCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "jaxb-load-xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load all data from .xml file by JAX-B";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOAD JAXB-XML DATA]");
        if(serviceLocator == null) return;
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, new HashMap());
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        @NotNull final File domainFile = new File(workingFolder, "domain.xml");
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(domainFile);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        serviceLocator.setCurrentUser(serviceLocator.getGuestUser());
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }
}
