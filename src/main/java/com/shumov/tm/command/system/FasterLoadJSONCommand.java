package com.shumov.tm.command.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shumov.tm.api.service.IDomainService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.domain.Domain;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public class FasterLoadJSONCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "faster-load-json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load all data from .json file by Faster";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOAD FASTER-JSON DATA]");
        if(serviceLocator == null) return;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        @NotNull final File domainFile = new File(workingFolder, "domain.json");
        @NotNull final Domain domain = objectMapper.readValue(domainFile,Domain.class);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        serviceLocator.setCurrentUser(serviceLocator.getGuestUser());
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }
}
