package com.shumov.tm.command.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.shumov.tm.api.service.IDomainService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.domain.Domain;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class FasterSaveJSONCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "faster-save-json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save all data in .json file by Faster";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVE FASTER-JSON DATA]");
        if(serviceLocator == null) return;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);
        @NotNull final String json = objectWriter.writeValueAsString(domain);
        @NotNull final byte[] data = json.getBytes(StandardCharsets.UTF_8);
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        if (!workingFolder.exists()) workingFolder.mkdir();
        @NotNull final File domainFile = new File(workingFolder, "domain.json");
        Files.write(domainFile.toPath(), data);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }
}
