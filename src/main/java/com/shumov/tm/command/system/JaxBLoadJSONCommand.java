package com.shumov.tm.command.system;

import com.shumov.tm.api.service.IDomainService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.domain.Domain;
import com.shumov.tm.enumerate.UserRoleType;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class JaxBLoadJSONCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "jaxb-load-json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load all data from .json file by JAX-B";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOAD JAXB-JSON DATA]");
        if(serviceLocator == null) return;
        final @NotNull Map<String, Object> properties = new HashMap<>();
        properties.put("eclipselink.media-type", "application/json");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        @NotNull final File domainFile = new File(workingFolder, "domain.json");
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(domainFile);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        serviceLocator.setCurrentUser(serviceLocator.getGuestUser());
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }
}
