package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.comparator.entity.TaskComparator;
import com.shumov.tm.enumerate.SortMethod;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;


public class TaskListSortedCommand extends AbstractCommand {

    @NotNull
    private final DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    @Override
    public @NotNull String command() {
        return "task-list-sorted";
    }

    @Override
    public @NotNull String getDescription() {
        return "Sorted task list";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK SORTED LIST]");
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[How do you want to sort the list?]".toUpperCase());
        System.out.println("create : sort by create date");
        System.out.println("start : sort by start date");
        System.out.println("finish : sort by finish date");
        System.out.println("status : sort by start date");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String method = terminalService.nextLine();
        @NotNull final User user = serviceLocator.getCurrentUser();
        @NotNull final List<Task> list = taskService.getTaskList(user.getId());
        sort(method,list);
        for (@NotNull final Task task : list) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName() +
                    "\nPROJECT ID: "+task.getIdProject()+
                    "\nDATE CREATE: " + formatter.format(task.getDateCreated()) +
                    "\nDATE START: " + formatter.format(task.getDateStart()) +
                    "\nDATE FINISH: " + formatter.format(task.getDateFinish()) +
                    "\nSTATUS: " + task.getStatus().toString()+"\n");
        }
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void sort(@NotNull final String method,
                      @NotNull final List<Task> list){
        switch (method.toLowerCase()){
            case "create" :
                list.sort(new TaskComparator(SortMethod.BY_CREATE_DATE));
                break;
            case "start" :
                list.sort(new TaskComparator(SortMethod.BY_START_DATE));
                break;
            case "finish" :
                list.sort(new TaskComparator(SortMethod.BY_FINISH_DATE));
                break;
            case "status" :
                list.sort(new TaskComparator(SortMethod.BY_STATUS));
                break;
            default:
                System.out.println("[you have not selected a sorting method]".toUpperCase());
        }
    }
}
