package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class UserUpdPassCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user-pass";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Set new password for current user";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[SET NEW PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.isCorrectPass(password);
        @NotNull final User user = serviceLocator.getCurrentUser();
        user.setPassword(password);
        userService.mergeUser(user);
        System.out.println("Password changed successfully".toUpperCase());
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
        roleTypes.add(UserRoleType.USER);
    }
}
