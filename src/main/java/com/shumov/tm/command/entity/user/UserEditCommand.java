package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class UserEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user-edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit user description";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[EDIT USER DESCRIPTION]");
        System.out.println("ENTER NEW DESCRIPTION:");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.isCorrectDescription(description);
        @NotNull final User user = serviceLocator.getCurrentUser();
        user.setDescription(description);
        userService.mergeUser(user);
        System.out.println("Description changed successfully".toUpperCase());
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}
