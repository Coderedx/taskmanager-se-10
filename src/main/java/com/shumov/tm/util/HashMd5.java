package com.shumov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashMd5 {

    public static String getMd5(@NotNull final String password) {
        @Nullable MessageDigest messageDigest = null;
        @NotNull byte[] digest = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(password.getBytes("UTF-8"));
            digest = messageDigest.digest();

        }catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        @NotNull BigInteger bigInt = new BigInteger(1, digest);
        @NotNull String md5Hex = bigInt.toString(16);
        while(md5Hex.length() < 32 ){
            md5Hex = "0" + md5Hex;
        }
        return md5Hex;
    }
}
