package com.shumov.tm.service;

import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Task;
import com.shumov.tm.repository.TaskRepository;
import com.shumov.tm.enumerate.ExecutionStatus;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public class TaskService extends AbstractService implements ITaskService {

    @NotNull
    protected IRepository<Task> repository = new TaskRepository();

    public TaskService(@NotNull final IRepository<Task> repository) {
        this.repository = repository;
    }

    @Override
    public void createTask(
        @Nullable final String ownerId,
        @Nullable final String name,
        @Nullable final String projectId
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(name);
        isCorrectInputData(projectId);
        @NotNull final Task task = new Task(ownerId, name, projectId);
        repository.persist(task);
    }

    @Override
    @NotNull
    public List<Task> getTaskList() throws Exception {
        return repository.findAll();
    }

    @Override
    public void setTasks(@NotNull final List<Task> tasks) throws Exception{
        repository.removeAll();
        for (@NotNull final Task task : tasks){
            isCorrectObject(task);
            repository.persist(task);
        }
    }

    @Override
    @NotNull
    public List<Task> getTaskList(@Nullable final String ownerId) throws Exception {
        isCorrectInputData(ownerId);
        return repository.findAll(ownerId);
    }

    @Override
    @NotNull
    public Task getTask(@Nullable final String id) throws Exception {
        isCorrectInputData(id);
        return repository.findOne(id);
    }

    @Override
    @NotNull
    public Task getTask(@Nullable final String ownerId, @Nullable final String id) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(id);
        return repository.findOne(ownerId, id);
    }

    @Override
    @NotNull
    public List<Task> getProjectTasks(@Nullable final String projectId) throws Exception {
        isCorrectInputData(projectId);
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull final Task task : repository.findAll()) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(projectId.equals(taskProjectId)){
                tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> getProjectTasks(
        @Nullable final String ownerId,
        @Nullable final String projectId
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(projectId);
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull final Task task : repository.findAll(ownerId)) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(projectId.equals(taskProjectId)){
                tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    public void clearData() throws Exception {
        repository.removeAll();
    }

    @Override
    public void clearData(@Nullable final String ownerId) throws Exception{
        isCorrectInputData(ownerId);
        repository.removeAll(ownerId);
    }

    @Override
    public void editTaskNameById(
        @Nullable final String id,
        @Nullable final String name
    ) throws Exception {
        isCorrectInputData(id);
        isCorrectInputData(name);
        @NotNull final Task task = repository.findOne(id);
        task.setName(name);
        repository.merge(id, task);
    }

    @Override
    public void editTaskNameById(
        @Nullable final String ownerId,
        @Nullable final String id,
        @Nullable final String name
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(id);
        isCorrectInputData(name);
        @NotNull final Task task = repository.findOne(ownerId, id);
        task.setName(name);
        repository.merge(id, task);
    }

    @Override
    public void editTaskDate(
        @Nullable String ownerId,
        @Nullable String id,
        @Nullable String start,
        @Nullable String finish
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(id);
        @NotNull final Date dateStart = parseDate(start);
        @NotNull final Date dateFinish = parseDate(finish);
        @NotNull final Task task = repository.findOne(ownerId, id);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        repository.merge(id, task);
    }

    @Override
    public void editTaskStatus(
        @Nullable String ownerId,
        @Nullable String id,
        @NotNull ExecutionStatus status
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(id);
        @NotNull final Task task = repository.findOne(ownerId, id);
        task.setStatus(status);
        repository.merge(id, task);
    }

    @Override
    public void removeTaskById(@Nullable final String id) throws Exception{
        isCorrectInputData(id);
        repository.remove(id);
    }

    @Override
    public void removeTaskById(
        @Nullable final String ownerId,
        @Nullable final String id
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(id);
        repository.remove(ownerId, id);
    }

    @Override
    public void addTaskInProject(
        @Nullable final String id,
        @Nullable final Task task
    ) throws Exception {
        isCorrectInputData(id);
        isCorrectObject(task);
        repository.merge(id,task);
    }

    @Override
    public void addTaskInProject(
        @Nullable final String ownerId,
        @Nullable final String id,
        @Nullable final Task task
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(id);
        isCorrectObject(task);
        repository.findOne(ownerId, id);
        repository.merge(id,task);
    }

    @Override
    @NotNull
    public ExecutionStatus taskStatus(@NotNull final String status) throws Exception  {
        switch (status.toLowerCase()){
            case "planned" : return ExecutionStatus.PLANNED;
            case "progress" : return ExecutionStatus.IN_PROGRESS;
            case "done" : return ExecutionStatus.DONE;
            default:
                throw new IOException("[you have not selected a status]".toUpperCase());
        }
    }
}
