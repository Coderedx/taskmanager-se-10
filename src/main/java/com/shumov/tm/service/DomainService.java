package com.shumov.tm.service;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.entity.domain.Domain;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class DomainService implements com.shumov.tm.api.service.IDomainService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public DomainService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void load(@Nullable final Domain domain) throws Exception {
        if (domain == null) return;
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        if(domain.getProjects() == null) projectService.setProjects(new ArrayList<>());
        else projectService.setProjects(domain.getProjects());
        if(domain.getTasks() == null) taskService.setTasks(new ArrayList<>());
        else taskService.setTasks(domain.getTasks());
        if(domain.getUsers() == null) userService.setUsers(new ArrayList<>());
        else userService.setUsers(domain.getUsers());
    }

    @Override
    public void export(@Nullable final Domain domain) throws Exception {
        if (domain == null) return;
        @NotNull final String userId = serviceLocator.getCurrentUser().getId();
        @NotNull final List<User> userList = serviceLocator.getUserService().getUserList();
        @NotNull final List<Project> projectList = serviceLocator.getProjectService().getProjectList();
        @NotNull final List<Task> taskList = serviceLocator.getTaskService().getTaskList();

        domain.setUsers(userList);
        domain.setProjects(projectList);
        domain.setTasks(taskList);
    }

}
